export default function({store, redirect, app}){
  if(!store.state.auth.authenticated){
    return redirect('/login');
  }else{
    app.$axios.defaults.headers.common = {'x-auth-token': `Bearer ${store.state.auth.token}`};
  }
}
