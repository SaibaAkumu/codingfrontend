export const state = function(){
  return {
    token: "",
    user: [],
    authenticated: false,
    error: "",
  };
};

export const actions = {
  login({commit}, {email, password}){
    this.$axios.post('login', {
      email: email,
      password: password,
    }).then((res) =>{
      commit('setAll', res.data)
    })
  },
  logout({commit}){
    commit('logout')
  }
}

export const mutations = {
  setAll(state, data){
    state.token = data.token;
    state.user = data.user;
    state.authenticated = data.status;
    state.error = "";
    if(state.authenticated){
      this.$router.push('/admin');
    }else{
      state.error = "login errato.";
    }
  },
  logout(state){
    state.authenticated = false;
    this.$router.push('/login');

  }
}
